package com.rabbit.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.rabbit.game.RabbitHuntingGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Rabbit-Hunting";
		config.width = 1080;
		config.height = 760;
		new LwjglApplication(new RabbitHuntingGame(), config);
	}
}
