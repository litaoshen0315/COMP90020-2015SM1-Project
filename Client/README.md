# Rabbit-Hunting Game #

This is the project for COMP90020 Distributed Algorithms at the University of Melbourne in 2015 Semester 1.

Rabbit hunting game is an implementation of central-server algorithm that used to allocate resources to up to 4 players. 

## Authors ##

* Litao Shen

* Xun Guo
