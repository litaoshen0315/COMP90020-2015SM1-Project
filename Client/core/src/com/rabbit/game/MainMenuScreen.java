package com.rabbit.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by litaoshen on 19/04/2015.
 */
public class MainMenuScreen implements Screen {

    private final RabbitHuntingGame game;

    public Stage stage = new Stage();
    public Table table = new Table();

    public Label.LabelStyle style;
    private Label title;
    private TextButton buttonPlay, buttonExit;
    private TextButton.TextButtonStyle textButtonStyle;
    private Skin buttonSkin, backgroundSkin;
    private TextureAtlas buttonAtlas, backgroundAtlas;

    FreeTypeFontGenerator generator;

    private MenuButtonClickedListener buttonClickedListener;

    public MainMenuScreen( RabbitHuntingGame game ) {
        this.game = game;
//

        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 120;
        BitmapFont font = generator.generateFont(parameter);

//        BitmapFont font = new BitmapFont();
        buttonSkin = new Skin();
        buttonAtlas = new TextureAtlas(Gdx.files.internal("skins/buttonSkin.pack"));
        buttonSkin.addRegions(buttonAtlas);
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        textButtonStyle.font.setScale( 0.8f );
        textButtonStyle.up = buttonSkin.getDrawable("button_up");
        textButtonStyle.down = buttonSkin.getDrawable("button_down");
//        textButtonStyle.checked = buttonSkin.getDrawable("checked-button");
        buttonPlay = new TextButton("Play", textButtonStyle);
        buttonPlay.getLabel().setAlignment(Align.center);
        buttonExit = new TextButton("Exit", textButtonStyle);
        buttonExit.getLabel().setAlignment(Align.center);

//        backgroundSkin = new Skin();
//        backgroundAtlas = new TextureAtlas(Gdx.files.internal("skins/background.pack"));
//        backgroundSkin.addRegions(backgroundAtlas);
//        table.setBackground(backgroundSkin.getDrawable("background"));
        table.background(new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("skins/moon_wolf_background.png")))));

        style = new Label.LabelStyle( font, Color.WHITE);
        title = new Label( "Rabbit Hunting", style );
//        title.setFontScale(0.9f);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

//        textButtonStyle.font.setScale( );
        title.setSize( width - 50, title.getY() );
        buttonPlay.setSize( width/4, height/4 );
        buttonExit.setSize( width/4, height/4 );

    }

    @Override
    public void show() {
        buttonPlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                ((Game)Gdx.app.getApplicationListener()).setScreen(new GameScreen( game ));
                try {
                    buttonClickedListener.onPlayButtonClicked();
                } catch ( GdxRuntimeException e ) {
                    e.printStackTrace();
                }
            }
        });
        buttonExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
//                System.exit(0);
            }
        });

        //The elements are displayed in the order you add them.
        //The first appear on top, the last at the bottom.
        table.add(title).padBottom(40).row();
        table.add(buttonPlay).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 6).padBottom(20).row();
        table.add(buttonExit).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 6).padBottom(20).row();

        table.setFillParent(true);
        table.align(Align.top);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        buttonSkin.dispose();
        buttonAtlas.dispose();
//        backgroundSkin.dispose();
        generator.dispose();

    }

    public void bindListener( Object object ){

        try {

            this.buttonClickedListener = (MenuButtonClickedListener) object;

        } catch ( ClassCastException e) {
            throw new ClassCastException(object.toString()
                    + " must implement MenuButtonClickedListener ");
        }
    }

    public interface MenuButtonClickedListener {

        void onPlayButtonClicked();
        void onExitButtonClicked();

    }
}
