package com.rabbit.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.rabbit.game.client.Position;
import com.rabbit.game.players.Enemy;
import com.rabbit.game.players.Player;
import com.rabbit.game.units.Bridge;
import com.rabbit.game.units.Target;

import java.util.ArrayList;

/**
 * Created by litaoshen on 19/04/2015.
 */
public class GameScreen implements Screen {

    private OnPlayerMovedListener onPlayerMovedListener;
    private OnResourceAcquiredListener onResourceAcquiredListener;

    public final RabbitHuntingGame game;
    private Player player;
    private ArrayList<Bridge> bridgeArrayList = new ArrayList<Bridge>();
    private ArrayList<Target> targetArrayList = new ArrayList<Target>();
    private ArrayList<Enemy> enemyArrayList = new ArrayList<Enemy>();

    public boolean gameEnd = false;

    private TiledMap map;
    private TiledMapTileLayer collisionLayer;
    private OrthogonalTiledMapRenderer renderer;


    private OrthographicCamera camera;

    public BitmapFont font;


    /**
     *
     */
    private InputAdapter inputAdapter = new InputAdapter() {
        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            Vector3 clickCoordinates = new Vector3(screenX, screenY, 0);
            Vector3 position = camera.unproject(clickCoordinates);
            player.setTarget_pos(new Vector3(position.x, position.y, 0));
            return true;
        }
    };


    public GameScreen(RabbitHuntingGame game,
                      Position initPlayerPosition,
                      int player_count,
                      int target_count) {


        this.game = game;

        // init font
        font = new BitmapFont();

        // create the camera and the SpriteBatch
        float w = Gdx.graphics.getWidth() / 2f;
        float h = Gdx.graphics.getHeight() / 2f;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);

        map = new TmxMapLoader().load("maps/map.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);

        collisionLayer = (TiledMapTileLayer) map.getLayers().get(0);
        // create player
        player = Player.createPlayer(initPlayerPosition.getPlayer_id(),
                initPlayerPosition.getX() * collisionLayer.getTileWidth(),
                initPlayerPosition.getY() * collisionLayer.getTileHeight(),
                collisionLayer);

        player.bindResourceAcquiredListener(this.pOnResourceAcquiredListener);

        camera.position.x = player.getX();
        camera.position.y = player.getY();

        // init bridges
        initBridges();

        // init targets
        initTargets(target_count);

        // init enemies
        initEnemies(player_count);

    }

    /**
     *
     */
    private void initBridges() {

        this.bridgeArrayList = Constants.initBridges();
    }

    /**
     * @param target_count
     */
    private void initTargets(int target_count) {

        // create targets
        ArrayList<Position> targetPositions = Constants.initTargetPositions();
        for (int i = 0; i < target_count; i++) {

            Position position = targetPositions.get(i);
            float cell_x = position.getX(), cell_y = position.getY();
            Target target = Target.createTarget(i,
                    cell_x * collisionLayer.getTileWidth(),
                    cell_y * collisionLayer.getTileHeight());

            targetArrayList.add(target);
        }
    }

    /**
     * @param player_count
     */
    private void initEnemies(int player_count) {

        // create enemies
        ArrayList<Position> enemyPositions = Constants.initPlayerPositions();
        for (int i = 1; i <= player_count; i++) {
            if (i != player.get_id()) {

                Position position = enemyPositions.get(i - 1);
                float cell_x = position.getX(), cell_y = position.getY();
                Enemy enemy = Enemy.createEnemy(i,
                        cell_x * collisionLayer.getTileWidth(),
                        cell_y * collisionLayer.getTileHeight());

                enemyArrayList.add(enemy);
            }
        }

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputAdapter);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.position.x = player.getX();
        camera.position.y = player.getY();

        // tell the camera to update its matrices
        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera
        game.getBatch().setProjectionMatrix(camera.combined);

        // render map
        renderer.setView(camera);
        renderer.render();

        // render player
        game.getBatch().begin();
        // update target list after player hunt
        targetArrayList = player.hunt(targetArrayList);
        if (onResourceAcquiredListener != null) {
            onResourceAcquiredListener.onTargetAcquired(targetArrayList);
        }

        // draw targets
        for (Target target : this.getTargetArrayList()) {

//            Gdx.app.log(" target status : ", String.valueOf(target.isCatched()));

            if (!target.isCatched()) {
                target.draw(game.getBatch());
            }
        }

        // draw enemies
        for (Enemy enemy : this.getEnemyArrayList()) {
            enemy.draw(game.getBatch());
        }

        // update player
        player.update(Gdx.graphics.getDeltaTime(), targetArrayList, bridgeArrayList);
        // pass player's position to position client to sync;
        if (onPlayerMovedListener != null) {
            onPlayerMovedListener.onPlayerMoved(this);
        }


        player.draw(game.getBatch());

        // Display winner information when game end.
        if (isGameEnd()) {
            Gdx.app.debug("Game End", String.valueOf(isGameEnd()));
            font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            font.setScale(2.0f);
            font.draw(game.getBatch(), getPlayer().isWin() ? "You win !!" : "You lose !!"
                    + getPlayer().getScore(), camera.position.x, camera.position.y + getPlayer().getHeight() + 3);

            EndGameScreen endGameScreen = new EndGameScreen(game, getPlayer().isWin());
            ((Game) Gdx.app.getApplicationListener()).setScreen(endGameScreen);
        }

        displayScore();

        game.getBatch().end();

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Displaying player's score
     */
    private void displayScore() {
        // Display player's score.
        font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        font.draw(game.getBatch(), "score : " + this.getPlayer().getScore(), camera.position.x, camera.position.y - 2);
    }

    @Override
    public void resize(int width, int height) {

        camera.viewportHeight = height;
        camera.viewportWidth = width;
        camera.update();

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        map.dispose();
        renderer.dispose();
    }

    public Player getPlayer() {
        return player;
    }

    public synchronized ArrayList<Enemy> getEnemyArrayList() {
        return enemyArrayList;
    }

    public synchronized void setEnemyArrayList(ArrayList<Enemy> enemyArrayList) {
        this.enemyArrayList = enemyArrayList;
    }

    public ArrayList<Bridge> getBridgeArrayList() {
        return bridgeArrayList;
    }

    public void setBridgeArrayList(ArrayList<Bridge> bridgeArrayList) {
        this.bridgeArrayList = bridgeArrayList;
    }

    public synchronized ArrayList<Target> getTargetArrayList() {
        return targetArrayList;
    }

    public synchronized void setTargetArrayList(ArrayList<Target> targetArrayList) {
        this.targetArrayList = targetArrayList;
    }

    public synchronized boolean isGameEnd() {
        return gameEnd;
    }

    public synchronized void setGameEnd(boolean gameEnd) {
        this.gameEnd = gameEnd;
    }

    /**
     *
     */
    public interface OnPlayerMovedListener {
        void onPlayerMoved(GameScreen gameScreen);
    }


    /**
     *
     */
    public interface OnResourceAcquiredListener {

        void onTargetAcquired(ArrayList<Target> targets);

        void onBridgeAcquired(Bridge bridge);

        void onBridgeLeave(Bridge bridge);

        void onBridgeDisgard(Bridge bridge);
    }

    public TiledMapTileLayer getCollisionLayer() {
        return collisionLayer;
    }

    /**
     * @param object
     */
    public void bindPlayerMoveListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.onPlayerMovedListener = (OnPlayerMovedListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }

    /**
     * @param object
     */
    public void bindResourceAcquiredListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.onResourceAcquiredListener = (OnResourceAcquiredListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }

    Player.OnResourceAcquiredListener pOnResourceAcquiredListener = new Player.OnResourceAcquiredListener() {
        @Override
        public void onTargetAcquired(ArrayList<Target> targets) {
        }

        @Override
        public void onBridgeAcquired(Bridge bridge) {
            if (onResourceAcquiredListener != null) {
                onResourceAcquiredListener.onBridgeAcquired(bridge);
            }
        }

        @Override
        public void onBridgeLeave(Bridge bridge) {
            if (onResourceAcquiredListener != null) {
                onResourceAcquiredListener.onBridgeLeave(bridge);
            }
        }

        @Override
        public void onBridgeDisgard(Bridge bridge) {
            if (onResourceAcquiredListener != null) {
                onResourceAcquiredListener.onBridgeDisgard(bridge);
            }
        }
    };
}
