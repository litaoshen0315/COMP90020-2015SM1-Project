package com.rabbit.game.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by litaoshen on 26/04/2015.
 */
public class Target extends Sprite {

    private boolean catched;
    private int _id;
    private boolean beAcquired;

    public static Target createTarget( int id, float x, float y ){

        Texture texture = new Texture(Gdx.files.internal("target.png"));

        Target target = new Target(texture, id);

        target.setPosition(x, y);

        return target;
    }

    public Target(Texture texture, int id) {
        super(texture);

        this.beAcquired = false;
        this.catched = false;
        this._id = id;

        this.setSize( 48, 72 );
        this.setBounds( getX(), getY(), getWidth(), getHeight() );
    }

    public int get_id() {
        return _id;
    }

    public boolean isCatched() {
        return catched;
    }

    public void setCatched(boolean catched) {
        this.catched = catched;
    }

    public boolean isBeAcquired() {
        return beAcquired;
    }

    public synchronized void setBeAcquired(boolean beAcquired) {
        this.beAcquired = beAcquired;
    }
}
