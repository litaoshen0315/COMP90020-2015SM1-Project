package com.rabbit.game.units;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by lanceguo on 24/04/15.
 */
public class Bridge {


    private int _id;
    private Vector2 end1;
    private Vector2 end2;

    public static Bridge createBridge(int id, float end1X, float end1Y,
                                      float end2X, float end2Y) {

        Bridge bridge = new Bridge(id, end1X, end1Y, end2X, end2Y);

        return bridge;
    }

    public Bridge(int id, float end1X, float end1Y,
                  float end2X, float end2Y) {

        this._id = id;

        this.end1 = new Vector2(end1X, end1Y);
        this.end2 = new Vector2(end2X, end2Y);
    }

    public boolean onBridgeEnd( int x, int y ) {

        boolean onEnd1 = Math.sqrt(Math.pow(x - this.getEnd1().x, 2) +
                Math.pow(y - this.getEnd1().y, 2)) <= 0.5;

        boolean onEnd2 = Math.sqrt(Math.pow(x - this.getEnd2().x, 2) +
                Math.pow(y - this.getEnd2().y, 2)) <= 0.5;


        return onEnd1 || onEnd2;
    }

    public int get_id() {
        return _id;
    }

    public Vector2 getEnd1() {
        return end1;
    }

    public Vector2 getEnd2() {
        return end2;
    }

}
