package com.rabbit.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

/**
 * Created by litaoshen on 2/05/2015.
 */
public class MyDialog extends Actor{

    Dialog dialog;


    public Dialog createMyDialog(String text) {

        try {

            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 120;
            BitmapFont font = generator.generateFont(parameter);

            Label.LabelStyle style = new Label.LabelStyle(font, Color.WHITE);

            Skin skin = new Skin(Gdx.files.internal("skins/uiskin.json"));

            Label label = new Label(text, style);
            label.setWrap(true);
//            label.setFontScale(1f);
            label.setAlignment(Align.center);

            dialog =
                    new Dialog("", skin) {
                        protected void result(Object object) {
                            System.out.println("Chosen: " + object);
                        }
                    };

            dialog.padTop(50).padBottom(50);
            dialog.getContentTable().add(label).width(850).row();
            dialog.getButtonTable().padTop(50);

            dialog.key(Input.Keys.ENTER, true).key(Input.Keys.ESCAPE, false);
            dialog.invalidateHierarchy();
            dialog.invalidate();
            dialog.layout();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dialog;
    }



    public void addbutton(String btnName, boolean checked, InputListener listener) {

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 120;
        BitmapFont font = generator.generateFont(parameter);

        Skin buttonSkin = new Skin();
        TextureAtlas buttonAtlas = new TextureAtlas(Gdx.files.internal("skins/buttonSkin.pack"));
        buttonSkin.addRegions(buttonAtlas);
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        textButtonStyle.up = buttonSkin.getDrawable("button_up");
        textButtonStyle.down = buttonSkin.getDrawable("button_down");
        TextButton dButton = new TextButton(btnName, textButtonStyle);

        dButton.addListener(listener);

        dialog.button(dButton, checked);
    }

    public void showDialog(Stage stage) {
        try {

            dialog.show(stage);
        } catch ( NullPointerException e ) {
            e.printStackTrace();
        }
    }

    public void discardDialog(){
        try {
            dialog.cancel();
        } catch ( NullPointerException e ) {
            e.printStackTrace();
        }
    }

}
