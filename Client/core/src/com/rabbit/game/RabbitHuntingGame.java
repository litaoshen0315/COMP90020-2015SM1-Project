package com.rabbit.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.rabbit.game.client.*;

public class RabbitHuntingGame extends Game {

    private SpriteBatch batch;
    private Client client;
    Stage stage;
    GameScreen world;

    private MainMenuScreen mainMenuScreen;
    private Boolean requestJoin = false;

    private MainMenuScreen.MenuButtonClickedListener menuButtonClickedListener =
            new MainMenuScreen.MenuButtonClickedListener() {
                @Override
                public void onPlayButtonClicked() {

                    try {

                        if (!requestJoin) {
                            requestJoin = true;


                            client = new Client( RabbitHuntingGame.this );

                            client.start();

                            client.bindListener(new Client.GameStatusListener() {

                                @Override
                                public void onConnectionFailed(Client client) {
//                                    Label text = new Label("Connection failed, please re-open game ...", client.getMainMenuScreen().style);
                                    Label text = new Label("Connection failed, please re-open game ...", mainMenuScreen.style);
                                    text.setFontScale(0.5f);
//                                    client.getMainMenuScreen().table.add(text).padBottom(40).row();
                                    mainMenuScreen.table.add(text).padBottom(40).row();
                                }

                                @Override
                                public void onGameJoined(Client client) {
                                    try {
//                                        Label text = new Label("Please wait for other players to join ...", client.getMainMenuScreen().style);
                                        Label text = new Label("Please wait for other players to join ...", mainMenuScreen.style);
                                        text.setFontScale(0.5f);
                                        mainMenuScreen.table.add(text).padBottom(40).row();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onGameStarted(final Client client) {
                                    Gdx.app.postRunnable(new Runnable() {
                                        @Override
                                        public void run() {


                                            Position initPlayerPosition = new Position(client.getPlayer_id(),
                                                    client.getInitX(),
                                                    client.getInitY());

                                            world = new GameScreen(RabbitHuntingGame.this,
                                                    initPlayerPosition,
                                                    client.getPlayer_count(),
                                                    client.getTarget_count());

                                            ResourceClient.startResourceClient(client, world);

                                            PositionClient.startPositionClient(Constants.CLIENT_HOST,
                                                    Constants.POS_CLIENT_PORT,
                                                    world);

                                            setScreen(world);
                                        }
                                    });

                                }
                            });

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onExitButtonClicked() {

                }
            };

    @Override
    public void create() {
        batch = new SpriteBatch();
//        this.setScreen( new MainMenuScreen(this) );
//        this.setScreen( new GameScreen(this, new Position(1, 1.0f, 1.0f), 2, 11) );
//

        this.mainMenuScreen = new MainMenuScreen(this);
        mainMenuScreen.bindListener(menuButtonClickedListener);
        // start main menu screen
        ((Game) Gdx.app.getApplicationListener()).setScreen(mainMenuScreen);
        ConnectionSettings connectionSettings = new ConnectionSettings();
//
//        while (!connectionSettings.isIpOk() || !connectionSettings.isPortOk()) {
//
//            synchronized (connectionSettings) {
//                try {
//                    connectionSettings.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }


    }


    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    public SpriteBatch getBatch() {
        return batch;
    }

}
