package com.rabbit.game.players;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector3;
import com.rabbit.game.units.Bridge;
import com.rabbit.game.units.Target;

import java.util.ArrayList;

/**
 * Created by litaoshen on 20/04/2015.
 */
public class Player extends Sprite {

    private TiledMapTileLayer collisionLayer;

    private OnResourceAcquiredListener onResourceAcquiredListener;

    private int score;
    private boolean win;

    private Vector3 target_pos = new Vector3();
    private boolean complete = false, isTargetSet = false;
    private float oldX, oldY;
    private float nextX, nextY;
    private int _id;

    private Bridge onBridge;
    private Bridge acquiringBridge;

    public static Player createPlayer(int id, float x, float y, TiledMapTileLayer collisionLayer) {

        Texture player_texture = new Texture(Gdx.files.internal("player.png"));
        Player player = new Player(player_texture, collisionLayer);
        player.set_id(id);
        player.setPosition(x, y);

        return player;
    }
//
//    public static Player createPlayer(int id, int cell_x, int cell_y, TiledMapTileLayer collisionLayer) {
//
//        Texture player_texture = new Texture(Gdx.files.internal("player.png"));
//        Player player = new Player(player_texture, collisionLayer);
//        player.set_id(id);
//        player.setPosition( cell_x * collisionLayer.getTileWidth(),
//                cell_y * collisionLayer.getTileHeight() );
//
//        return player;
//    }

    private Player(Texture texture, TiledMapTileLayer collisionLayer) {
        super(texture);

        this.score = 0;
        this.setSize(48, 72);

        this.setBounds(getX(), getY(), getWidth(), getHeight());
        this.collisionLayer = collisionLayer;

    }


    public void update(float delta, ArrayList<Target> targets, ArrayList<Bridge> bridges) {

        if (complete || !isTargetSet) {
            return;
        }

        boolean xBlock = false, yBlock = false;
        oldX = getX();
        oldY = getY();

        float velocityX = (target_pos.x - oldX);
        float velocityY = (target_pos.y - oldY);

        nextX = oldX + velocityX * delta;
        nextY = oldY + velocityY * delta;

        boolean nextIsBridge = false;

        // check if next cell is blocked
        if (velocityY < 0) {
            this.setFlip(false, false);
            // check bottom
            if (isCellBlocked(nextX + this.getWidth() / 2, nextY, collisionLayer)) {
                yBlock = true;
            }

            if (isBridge(nextX + this.getWidth() , nextY - this.getHeight() / 4)) {
                if (this.getAcquiringBridge() != null ) {
                    yBlock = true;
                }
                nextIsBridge = true;
            }

            if (isBridge(nextX , nextY - this.getHeight() / 4)) {
                if (this.getAcquiringBridge() != null) {
                    yBlock = true;
                }
                nextIsBridge = true;
            }

        } else if (velocityY > 0) {

            this.setFlip(true, true);

            // check top
            if (isCellBlocked(nextX + this.getWidth() / 2, nextY + this.getHeight(), collisionLayer)) {
                yBlock = true;
            }

            if (isBridge(nextX + this.getWidth(), nextY + this.getHeight())) {
                if (this.getAcquiringBridge() != null) {
                    yBlock = true;
                }
                nextIsBridge = true;
            }

            if (isBridge(nextX, nextY + this.getHeight())) {
                if (this.getAcquiringBridge() != null) {
                    yBlock = true;
                }
                nextIsBridge = true;
            }

        }

        if (!yBlock) {
            setY(nextY);
        }


        if (velocityX < 0) {
            // check left
            if (isCellBlocked(nextX, nextY + this.getHeight() / 2, collisionLayer)) {
                xBlock = true;
            }

            if (isBridge(nextX, nextY + this.getHeight() / 2)) {

                if (this.getAcquiringBridge() != null) {
                    xBlock = true;
                }
                nextIsBridge = true;
            }
        } else if (velocityX > 0) {

            // check right
            if (isCellBlocked(nextX + this.getWidth(), nextY + this.getHeight() / 2, collisionLayer)) {
                xBlock = true;
            }

            if (isBridge(nextX + this.getWidth(), nextY + this.getHeight() / 2)) {

                if (this.getAcquiringBridge() != null) {
                    xBlock = true;
                }
                nextIsBridge = true;
            }
        }


        if (!xBlock) {
            setX(nextX);
        }

//        Gdx.app.error("Player position : ", String.valueOf(this.getCellX()) + "," +
//                String.valueOf(this.getCellY()));
        // Todo: acquiring bridge and leaving bridge;
        // check if player is acquiring/leave bridge
        for (Bridge bridge : bridges) {

            boolean isAccepted = this.getOnBridge() != null
                    && this.getOnBridge().get_id() == bridge.get_id(),
                    onBridgeEnd = bridge.onBridgeEnd(this.getCellX(), this.getCellY());
//
//            if( bridge.get_id() == 0 ) {
//                Gdx.app.error("Bridge: ", String.valueOf(bridge.get_id()));
//                Gdx.app.error("Player acquiring bridge : ",
//                        String.valueOf(this.getAcquiringBridge() == null? "null": this.getAcquiringBridge().get_id()));
//                Gdx.app.error("Player on bridge : ", String.valueOf(isAccepted));
//                Gdx.app.error("Bridge End : ", String.valueOf(onBridgeEnd));
//                Gdx.app.error("Bridge next step : ", String.valueOf(nextIsBridge));
//                Gdx.app.error("----------", "--------");
//            }

            if (!isAccepted
                    && onBridgeEnd
                    && nextIsBridge) {

                this.setAcquiringBridge(bridge);

                if (onResourceAcquiredListener != null) {

                    onResourceAcquiredListener.onBridgeAcquired(bridge);

                }
            } else if (isAccepted
                    && isBridge(this.getX(), this.getY())
                    && !nextIsBridge) {

                this.setAcquiringBridge(null);
                if (onResourceAcquiredListener != null) {

                    onResourceAcquiredListener.onBridgeLeave(bridge);

                }
            } else if ( isAccepted
                    && !isBridge(this.getX(), this.getY())
                    && !nextIsBridge) {

                // get accept but not going onto bridge
                if (onResourceAcquiredListener != null) {

                    onResourceAcquiredListener.onBridgeLeave(bridge);

                }
            } else if (isAcquiringBridge(bridge)
                    && !nextIsBridge) {
                // TODO: check next movement of player and bridge status

                this.setAcquiringBridge(null);
                if (onResourceAcquiredListener != null) {

                    onResourceAcquiredListener.onBridgeDisgard(bridge);
                }
            }
        }

        // if reach target position, stop moving.
        if (Math.sqrt(Math.pow(getX() - target_pos.x, 2) + Math.pow(getY() - target_pos.y, 2)) <= 0.02) {
            isTargetSet = false;
            complete = true;
        }
    }

    /**
     * check if is the bridge that acquired by player
     *
     * @param bridge
     * @return
     */
    private boolean isAcquiringBridge(Bridge bridge) {

        return this.getAcquiringBridge() != null
                && this.getAcquiringBridge().get_id() == bridge.get_id();
    }

    public boolean isBridge(float x, float y) {
        TiledMapTileLayer.Cell cell = null;

        try {
            cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()),
                    (int) (y / collisionLayer.getTileHeight()));

//            Gdx.app.log("Cell position : ",
//                    String.valueOf((int) (x / collisionLayer.getTileWidth())) + " , "
//                            + String.valueOf((int) (y / collisionLayer.getTileHeight())));
//            Gdx.app.log("Cell position in map : ",
//                    String.valueOf(x) + " , "
//                            + String.valueOf(y));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (cell != null && cell.getTile().getProperties().containsKey("bridge"));
    }

    public boolean isCellBlocked(float x, float y, TiledMapTileLayer collisionLayer) {

        TiledMapTileLayer.Cell cell = null;

        try {
            cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()),
                    (int) (y / collisionLayer.getTileHeight()));

//            Gdx.app.log("Cell position : ",
//                    String.valueOf((int) (x / collisionLayer.getTileWidth())) + " , "
//                            + String.valueOf((int) (y / collisionLayer.getTileHeight())));
//            Gdx.app.log("Cell position in map : ",
//                    String.valueOf(x) + " , "
//                            + String.valueOf(y));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (cell != null && cell.getTile().getProperties().containsKey("block")) || cell == null;
    }

    /**
     * @param targetArrayList
     * @return
     */
    public ArrayList<Target> hunt(ArrayList<Target> targetArrayList) {

        ArrayList<Target> tList = new ArrayList<Target>();
        for (Target t : targetArrayList) {
//        Gdx.app.log("target - player distance: ", String.valueOf(distance(target)));
            if (distance(t) <= 0.5 && !t.isCatched()) {
                t.setBeAcquired(true);
            }
            tList.add(t);
        }

        return tList;

    }

    public void setTarget_pos(Vector3 target_pos) {
        this.target_pos = target_pos;
        this.isTargetSet = true;
        this.complete = false;
    }

    public double distance(Target target) {

        int targetCellX = (int) (target.getX() / collisionLayer.getTileWidth());
        int targetCellY = (int) (target.getY() / collisionLayer.getTileHeight());

        return Math.sqrt(Math.pow(getCellX() - targetCellX, 2)
                + Math.pow(getCellY() - targetCellY, 2));
    }

    public int getCellX() {
        return (int) (getX() / collisionLayer.getTileWidth());
    }

    public int getCellY() {

        return (int) (getY() / collisionLayer.getTileHeight());
    }

    public float getOldX() {
        return oldX;
    }

    public float getOldY() {
        return oldY;
    }

    public float getNextX() {
        return nextX;
    }

    public float getNextY() {

        return nextY;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public synchronized Bridge getOnBridge() {
        return onBridge;
    }

    public synchronized void setOnBridge(Bridge onBridge) {
        this.onBridge = onBridge;
    }

    public synchronized Bridge getAcquiringBridge() {
        return acquiringBridge;
    }

    public synchronized void setAcquiringBridge(Bridge acquiringBridge) {
        this.acquiringBridge = acquiringBridge;
    }

    /**
     *
     */
    public interface OnResourceAcquiredListener {

        void onTargetAcquired(ArrayList<Target> targets);

        void onBridgeAcquired(Bridge bridge);

        void onBridgeLeave(Bridge bridge);

        void onBridgeDisgard(Bridge bridge);
    }

    /**
     * @param object
     */
    public void bindResourceAcquiredListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.onResourceAcquiredListener = (OnResourceAcquiredListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }

}

