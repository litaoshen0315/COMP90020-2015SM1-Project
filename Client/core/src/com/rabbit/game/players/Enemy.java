package com.rabbit.game.players;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by litaoshen on 4/05/2015.
 */
public class Enemy extends Sprite {

    private int _id;
    private int score;
    private boolean connected;


    public static Enemy createEnemy( int id, float x, float y ) {


        Texture texture = new Texture(Gdx.files.internal("enemy.png"));
        Enemy enemy = new Enemy(texture);
        enemy.set_id( id );
        enemy.setPosition(x, y);

        return enemy;
    }

    private Enemy(Texture texture) {
        super(texture);

        this.connected = true;
        this.setSize(48, 72);

        this.setBounds(getX(), getY(), getWidth(), getHeight());
    }


    @Override
    public void draw(Batch batch) {

        if( isConnected() ) {
            // update enemy when draw.
            update(Gdx.graphics.getDeltaTime());
            super.draw(batch);
        }
    }

    public void update( float delta ){
        if( this.getX() == -1 && this.getY() == -1 ){
            this.setConnected( false );
        }


    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public synchronized boolean isConnected() {
        return connected;
    }

    public synchronized void setConnected(boolean connected) {
        this.connected = connected;
    }
}
