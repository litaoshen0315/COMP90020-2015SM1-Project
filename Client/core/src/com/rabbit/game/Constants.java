package com.rabbit.game;

import com.rabbit.game.client.Position;
import com.rabbit.game.units.Bridge;

import java.util.ArrayList;

/**
 * Created by litaoshen on 19/04/2015.
 */
public class Constants {

    public static final float viewportWidth = 800;
    public static final float viewportHeight = 480;


    public static String CLIENT_HOST = "192.168.0.103";
    public static int CLIENT_PORT = 8000;

    public static final int POS_CLIENT_PORT = 3000;

    private static final int[] target_x = new int[]{1  , 1  , 9  , 5  , 2  , 9  , 45 , 35 , 46 , 32 , 37 , 24 , 20 , 14 , 9 , 7  , 28 , 29 , 39 , 45 , 27};
    private static final int[] target_y = new int[]{44 , 32 , 36 , 24 , 20 , 22 , 45 , 34 , 30 , 1  , 11 , 22 , 10 , 7  , 9 , 30 , 11 , 8  , 10 , 21 , 33};

    public static final int BRIDGE_COUNT = 12;

    private static final int[] end1_x = new int[]{5  , 7  , 15 , 17 , 19 , 24 , 24 , 30 , 37 , 41 , 45 , 45};
    private static final int[] end1_y = new int[]{19 , 18 , 21 , 18 , 21 , 17 , 22 , 20 , 16 , 16 , 15 , 24};

    private static final int[] end2_x = new int[]{5 , 7 , 15 , 17 , 19 , 24 , 24 , 30 , 37 , 41 , 45 , 45};
    private static final int[] end2_y = new int[]{22 , 27 , 27 , 18 , 21 , 21 , 25 , 26 , 24 , 18 , 18 , 24};

    /**
     *
     * @return
     */
    public static ArrayList<Position> initPlayerPositions(){
        ArrayList<Position> positions = new ArrayList<Position>();

        positions.add( new Position(1, 1, 0));
        positions.add( new Position(2, 46, 1));
        positions.add( new Position(3, 22, 47));
        positions.add( new Position(4, 25, 47));

        return positions;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Position> initTargetPositions(){

        if( target_x.length != target_y.length ) {
            return null;
        }

        ArrayList<Position> positions = new ArrayList<Position>();


        for( int i=0; i < target_x.length; i++ ) {
            Position position = new Position( i, target_x[i], target_y[i] );
            positions.add( position );
        }

        return positions;
    }

    public static ArrayList<Bridge> initBridges(){

        if( end1_x.length != end1_y.length
                || end2_x.length != end2_y.length
                || end1_x.length != end2_x.length ){

            return null;
        }

        ArrayList<Bridge> bridges = new ArrayList<Bridge>();

        for( int i = 0; i < BRIDGE_COUNT; i++ ){

            Bridge bridge = Bridge.createBridge(i, end1_x[i], end1_y[i],
                    end2_x[i], end2_y[i] );

            bridges.add( bridge );

        }

        return bridges;
    }

}
