package com.rabbit.game.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.rabbit.game.Constants;
import com.rabbit.game.MainMenuScreen;
import com.rabbit.game.RabbitHuntingGame;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Client extends Thread {

    public com.badlogic.gdx.net.Socket socket;

    public BufferedReader in;
    public PrintWriter out;

    private MainMenuScreen mainMenuScreen;

    private Boolean requestJoin = false;

    int player_count;
    int target_count;
    int player_id;
    float initX, initY;
    int score;

    boolean is_started;
    boolean is_winner;

    private GameStatusListener gameStatusListener;

    private MainMenuScreen.MenuButtonClickedListener menuButtonClickedListener =
            new MainMenuScreen.MenuButtonClickedListener() {
                @Override
                public void onPlayButtonClicked() {

                    try {

                        if (!requestJoin) {
                            requestJoin = true;
                            joinGame();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onExitButtonClicked() {

                }
            };


    public Client(RabbitHuntingGame game) {
        this.player_id = 0;
        this.score = 0;
        this.is_started = false;
        this.is_winner = false;
//
//        this.mainMenuScreen = new MainMenuScreen(game);
//        mainMenuScreen.bindListener(menuButtonClickedListener);
//        // start main menu screen
//        ((Game) Gdx.app.getApplicationListener()).setScreen(mainMenuScreen);

    }

    @Override
    public void run() {
        // try connecting to server
        try {
            SocketHints socketHints = new SocketHints();
//            socketHints.socketTimeout = 100;

            try {
                socket = Gdx.net.newClientSocket(Net.Protocol.TCP,
                        Constants.CLIENT_HOST,
                        Constants.CLIENT_PORT,
                        socketHints);
            } catch (GdxRuntimeException e) {


                if (gameStatusListener != null) {
                    gameStatusListener.onConnectionFailed(this);
                }
                Gdx.app.error(" Connecting Error: ",
                        " Cannot connect to server " + Constants.CLIENT_HOST);
            }

            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            // join game
            joinGame();
        } catch (Exception e) {
            e.printStackTrace();

        }


        while (!this.isInterrupted() && socket != null) {

            try {
                // If in waiting list, request the bridge 5 times
//			int j = 0;


                String received = this.in.readLine();
                JSONObject jObj = Protocol.parse(received);

                if (jObj == null) {
                    continue;
                }
                Gdx.app.log(" --------- Received : ", jObj.toJSONString() + " ---------");

                if (!is_started()) {
                    if (Protocol.checkType(jObj).equals("Joined")) {

                        this.player_id = Integer.parseInt(jObj.get("player_id").toString());

                        if (gameStatusListener != null) {
                            gameStatusListener.onGameJoined(this);
                        }

                    } else if (Protocol.checkType(jObj).equals("Start")) {
                        setStart(true);

                        this.player_count = Integer.parseInt(jObj.get("player_count").toString());
                        this.target_count = this.player_count * 5 + 1;
                        initX = Float.parseFloat(jObj.get("init_x").toString());
                        initY = Float.parseFloat(jObj.get("init_y").toString());

                        if (gameStatusListener != null) {
                            gameStatusListener.onGameStarted(this);
                        }

                        break;

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void joinGame() {

        Gdx.app.log("Try to join game", "");
        this.out.println(Protocol.joinGame());
    }


    public boolean is_started() {
        return this.is_started;
    }


    private void setStart(boolean started) {

        this.is_started = started;
    }

    public interface GameStatusListener {

        void onConnectionFailed(Client client);

        void onGameJoined(Client client);

        void onGameStarted(Client client);

    }

    public void bindListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.gameStatusListener = (GameStatusListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }


    public MainMenuScreen getMainMenuScreen() {
        return mainMenuScreen;
    }

    public int getPlayer_id() {
        return player_id;
    }

    public float getInitX() {
        return initX;
    }

    public float getInitY() {
        return initY;
    }

    public int getPlayer_count() {
        return player_count;
    }

    public int getTarget_count() {
        return target_count;
    }
}
