package com.rabbit.game.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.net.Socket;
import com.rabbit.game.GameScreen;
import com.rabbit.game.units.Bridge;
import com.rabbit.game.units.Target;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by litaoshen on 5/05/2015.
 */
public class ResourceClient extends Thread {

    private static ResourceClient resourceClient = null;

    Client client;
    Socket socket;

    BufferedReader in;
    PrintWriter out;

    GameScreen world;

    private OnReplyListener onReplyListener;
    private boolean isWaiting = false;
    private int request_count = 0;

    /**
     *
     */
    private GameScreen.OnResourceAcquiredListener onResourceAcquiredListener = new GameScreen.OnResourceAcquiredListener() {

        @Override
        public void onTargetAcquired(ArrayList<Target> targets) {
            for (Target target : targets) {
                if (!target.isCatched() && target.isBeAcquired()) {
                    acquireTarget(target.get_id());

//                    Gdx.app.error(" ----- Target acquired : ",
//                            String.valueOf(target.get_id()) + " ----- ");
                }
            }
        }

        @Override
        public void onBridgeAcquired(Bridge bridge) {

            if (!isWaiting || (isWaiting && request_count < 5)) {

                acquireBridge(bridge.get_id());
                Gdx.app.error(" ----- Bridge Acquired : ",
                        String.valueOf(bridge.get_id()) + " ----- ");
                request_count++;

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onBridgeLeave(Bridge bridge) {

            leaveBridge(bridge.get_id());
            Gdx.app.error(" ----- Bridge Leave : ",
                    String.valueOf(bridge.get_id()) + " ----- ");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onBridgeDisgard(Bridge bridge) {

            discardBridge(bridge.get_id());
            Gdx.app.error(" ----- Bridge discard : ",
                    String.valueOf(bridge.get_id()) + " ----- ");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };

    public static ResourceClient startResourceClient(Client client, GameScreen world) {

        if (resourceClient == null) {
            resourceClient = new ResourceClient(client, world);
            resourceClient.start();
        }

        return resourceClient;
    }

    private ResourceClient(Client client, GameScreen world) {
        this.client = client;
        this.socket = client.socket;
        this.in = client.in;
        this.out = client.out;
        this.world = world;

        this.world.bindResourceAcquiredListener(onResourceAcquiredListener);
    }

    @Override
    public void run() {

        int i = 0;
        // If in waiting list, request the bridge 5 times
        int j = 0;

        while (!this.isInterrupted() && socket != null) {

            try {

                if (!this.isInterrupted() && socket != null) {

                    String received = this.in.readLine();
                    JSONObject jObj = Protocol.parse(received);

                    if (jObj == null) {
                        continue;
                    }
//                    Gdx.app.log(" --------- Received : ", jObj.toJSONString() + " ---------");

                    // check header of json object
                    String header = Protocol.checkType(jObj);

                    // targets
                    if (header.equals("Target")) {
                        String result = jObj.get("target").toString();
                        if (result.equals("True")) {
                            // TODO: increment score of player get.
                            client.score++;

                            world.getPlayer().setScore(client.score);
                            Gdx.app.error("Player score, ", String.valueOf(client.score));

                            // let listener know
                            if (onReplyListener != null) {
                                onReplyListener.onTargetCatched(this);
                            }
                        }
                    }

                    // bridges
                    if (header.equals("Accept")) {


                        this.isWaiting = false;
                        this.request_count = 0;
                        int bridge_id = Integer.valueOf(jObj.get("bridge").toString());
                        // update player's status
                        world.getPlayer().setOnBridge(world.getBridgeArrayList().get(bridge_id));
                        world.getPlayer().setAcquiringBridge(null);
//                        Thread.sleep( 10 );

                        // let listener know
                        if (onReplyListener != null) {
                            onReplyListener.onBridgeAccept(this, bridge_id);
                        }
                    } else if (header.equals("Waiting")) {
                        int bridge_id = Integer.valueOf(jObj.get("bridge").toString());
                        this.isWaiting = true;

                        world.getPlayer().setOnBridge(null);
                        world.getPlayer().setAcquiringBridge(world.getBridgeArrayList().get(bridge_id));

                        // let listener know
                        if (onReplyListener != null) {
                            onReplyListener.onBridgeWait(this);
                        }
                    } else if (header.equals("Discarded")) {

                        this.isWaiting = false;
                        this.request_count = 0;

                        int bridge_id = Integer.valueOf(jObj.get("bridge").toString());
                        world.getPlayer().setOnBridge(null);

                    } else if (header.equals("Released")) {

                        // update player's status
//                        int bridge_id = Integer.valueOf(jObj.get("bridge").toString());
                        world.getPlayer().setOnBridge(null);
                        world.getPlayer().setAcquiringBridge(null);

                    }

                    // End of game
                    if (header.equals("End")) {
                        boolean win = false;
                        int winner_id = Integer.valueOf(jObj.get("winner").toString());
                        if (winner_id == client.player_id) {
                            win = true;
                        } else {
                            win = false;
                        }


                        world.setGameEnd(true);
                        world.getPlayer().setWin(win);
                        this.leaveGame();

                        if (onReplyListener != null) {
                            onReplyListener.onGameEnd(this, win);
                        }
                        break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
        }
    }

    public synchronized void acquireTarget(int id) {
        this.out.println(Protocol.acquireTarget(id));
    }

    public synchronized void acquireBridge(int id) {
        this.out.println(Protocol.sendRequest(id));
    }

    public synchronized void leaveBridge(int id) {
        this.out.println(Protocol.leaveBridge(id));
    }

    public synchronized void discardBridge(int id) {
        this.out.println(Protocol.discardRequest(id));
    }

    public synchronized GameScreen getWorld() {
        return world;
    }

    public void leaveGame() {
        Gdx.app.log("Try to leave game", "");
        this.out.println(Protocol.leaveGame(client.player_id));
    }


    public interface OnReplyListener {

        void onTargetCatched(ResourceClient resourceClient);

        void onBridgeAccept(ResourceClient resourceClient, int bridge_id);

        void onBridgeWait(ResourceClient resourceClient);

        void onGameEnd(ResourceClient resourceClient, boolean win);
    }

    public void bindResourceAcquiredListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.onReplyListener = (OnReplyListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }


}
