package com.rabbit.game.client;

import com.rabbit.game.GameScreen;
import com.rabbit.game.players.Enemy;
import com.rabbit.game.units.Target;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class PositionClient extends Thread {

    private static PositionClient positionClient = null;

    Position position;
    Socket socket;

    BufferedReader in;
    PrintWriter out;

    GameScreen world;

    int player_id;
    int player_count;
    ArrayList<Position> positions;
    ArrayList<Boolean> targets_catched;
//    int[] positions;

    private OnPositionListener onPositionListener;

    private GameScreen.OnPlayerMovedListener onPlayerMovedListener = new GameScreen.OnPlayerMovedListener() {
        @Override
        public void onPlayerMoved(GameScreen gameScreen) {

            if (socket != null
                    && !socket.isClosed()) {
                sendPosition();
            }
        }

    };

    /**
     * @param host
     * @param port
     * @param gameScreen
     * @return
     */
    public static PositionClient startPositionClient(String host, int port, GameScreen gameScreen) {

        if( positionClient == null ) {

            positionClient = new PositionClient(host, port, gameScreen);
            positionClient.start();
        }

        return positionClient;

    }

    /**
     * @param host
     * @param port
     * @param gameScreen
     */
    private PositionClient(String host, int port, GameScreen gameScreen) {

        this.world = gameScreen;

        this.position = new Position(player_id,
                world.getPlayer().getCellX(),
                world.getPlayer().getCellY());

        this.player_id = gameScreen.getPlayer().get_id();

        try {
            this.socket = new Socket(host, port);

            this.in = new BufferedReader(new InputStreamReader(
                    this.socket.getInputStream()));
            this.out = new PrintWriter(this.socket.getOutputStream(), true);

            this.out.println(Protocol.connectPosServer(world.getPlayer().getCellX(),
                    world.getPlayer().getCellY(),
                    world.getPlayer().get_id()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        // bind listener to monitor player's position change
        world.bindPlayerMoveListener(onPlayerMovedListener);

    }

    @Override
    public void run() {
        try {
            while (!this.isInterrupted() && socket != null
                    && !socket.isClosed()) {
                String received = this.in.readLine();
                JSONObject jObj = Protocol.parse(received);

//                Gdx.app.log("received updated status of position & target : ", jObj.toJSONString());

                if (Protocol.checkType(jObj).equals("Connected")) {

                    this.player_count = Integer.parseInt(jObj.get("player_count").toString());

                    if( onPositionListener != null ) {
                        onPositionListener.onConnected(this);
                    }

                } else if (Protocol.checkType(jObj).equals("Position")) {

                    positions = parsePositions(jObj);
                    targets_catched = parseTargetsStatus(jObj);

                    // update enemies' positions
                    ArrayList<Enemy> enemies = world.getEnemyArrayList();
                    for (int i = 1; i <= this.player_count; i++) {
                        for (int j = 0; j < enemies.size(); j++) {
                            if (i == enemies.get(j).get_id()) {
                                enemies.get(j).setPosition( positions.get(i-1).getX(),
                                        positions.get(i-1).getY() );
                            }
                        }
                    }
                    world.setEnemyArrayList(enemies);

                    // update targets' status
                    ArrayList<Target> targets = world.getTargetArrayList();
                    for (int i = 0; i < targets.size(); i++) {
                        targets.get(i).setCatched(targets_catched.get(i));
                    }
                    world.setTargetArrayList(targets);


                    if (onPositionListener != null) {
                        onPositionListener.onPositionsReceived(this);
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else if (Protocol.checkType(jObj).equals("Disconnect")) {
                    if (onPositionListener != null) {
                        onPositionListener.onDisconnected(this);
                    }
                    break;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                this.out.println(Protocol.leaveGame(this.player_id));
                this.socket.close();
                Thread.currentThread().interrupt();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnPositionListener {
        void onConnected(PositionClient positionClient);

        void onPositionsReceived(PositionClient positionClient);

        void onDisconnected(PositionClient positionClient);
    }


    public void bindListener(Object object) {
        try {
            // instantiate the onGameStartListener
            this.onPositionListener = (OnPositionListener) object;
        } catch (ClassCastException e) {
            // the clas doesn't implement the interface, throw exception
            throw new ClassCastException(object.toString()
                    + " must implement onGameStartListener");
        }
    }

    /**
     * parse JSON Object
     *
     * @param jObj
     * @return
     */
    private ArrayList<Position> parsePositions(JSONObject jObj) {

        ArrayList<Position> positions = new ArrayList<Position>(this.player_count);
        for (int i = 1; i <= this.player_count; i++) {
            String x_key = "x_pos" + i;
            String y_key = "y_pos" + i;

            Position position = new Position(i,
                    Float.parseFloat(jObj.get(x_key).toString()),
                    Float.parseFloat(jObj.get(y_key).toString()));
            positions.add(position);
        }

        return positions;
    }

    /**
     * parse JSON Object
     *
     * @param jObj
     * @return
     */
    private ArrayList<Boolean> parseTargetsStatus(JSONObject jObj) {

        int num_targets = Integer.parseInt(jObj.get("num_targets").toString());
        ArrayList<Boolean> targets_catched = new ArrayList<Boolean>();

        for (int t = 0; t < num_targets; t++) {

            String target_key = "target_status" + t;

            boolean isAvailable = Boolean.parseBoolean(jObj.get(target_key).toString());
            targets_catched.add(!isAvailable);

//            Gdx.app.log(" Target Status " + t + " - ", String.valueOf(!isAvailable));
        }

        return targets_catched;
    }


    public void sendPosition() {

        this.out.println(Protocol.sendPosition(world.getPlayer().getX(),
                world.getPlayer().getY()));
    }

    public ArrayList<Position> getPositions() {
        return positions;
    }
}
