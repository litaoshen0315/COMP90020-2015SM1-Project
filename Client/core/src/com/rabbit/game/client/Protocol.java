package com.rabbit.game.client;


import com.badlogic.gdx.Gdx;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Protocol {

    private static boolean DEBUG = false;
	@SuppressWarnings("unchecked")
    public static String sendPosition(float x, float y) {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Position");
        jObj.put("x_pos", x);
        jObj.put("y_pos", y);

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }
	
	@SuppressWarnings("unchecked")
	public static String connectPosServer(float x, float y, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "ConnectPos");
		jObj.put("player", player_id);
		jObj.put("init_x", x);
		jObj.put("init_y", y);
		
//		System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
		return jObj.toJSONString();
	}
	
    @SuppressWarnings("unchecked")
    public static String sendRequest(int bridge_id) {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Request");
        jObj.put("bridge", bridge_id);

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String discardRequest(int bridge_id) {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Discard");
        jObj.put("bridge", bridge_id);

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String leaveBridge(int bridge_id) {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Release");
        jObj.put("bridge", bridge_id);

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String joinGame() {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Ready");

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String acquireTarget(int target_id) {

        JSONObject jObj = new JSONObject();
        jObj.put("type", "Acquire");
        jObj.put("target", target_id);

//        System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
        return jObj.toJSONString();
    }
    
    @SuppressWarnings("unchecked")
	public static String leaveGame(int player_id) {
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("type", "Leave");
    	jObj.put("player", player_id);
    	
//    	System.out.println(jObj.toJSONString());
        debug_info( jObj.toJSONString() );
    	return jObj.toJSONString();
    }

    public static JSONObject parse(String msg) {

        JSONParser parser = new JSONParser();
        JSONObject jObj = null;
        if (msg != null) {
            try {
                jObj = (JSONObject) parser.parse(msg);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return jObj;

    }

    public static String checkType(JSONObject jObj) {

        String type = "";

        if (jObj != null) {
            type = (String) jObj.get("type");
        }

        return type;
    }

    private static void debug_info( String text ){
        if( DEBUG ) Gdx.app.log("-------- Send Json Message : " , text + " --------");
    }
}
