package com.rabbit.game.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.rabbit.game.Constants;

/**
 * Created by litaoshen on 25/05/2015.
 */
public class ConnectionSettings {

    boolean ipOk = false, portOk = false;

    String IP;
    int port;
    Input.TextInputListener ipInputListener = new Input.TextInputListener() {
        @Override
        public void input(String text) {
            Constants.CLIENT_HOST = text;
            IP = text;

            setIpOk(true);
            synchronized ( ConnectionSettings.this ) {
                ConnectionSettings.this.notify();
            }
        }

        @Override
        public void canceled() {
            setIpOk(true);
            synchronized ( ConnectionSettings.this ) {
                ConnectionSettings.this.notify();
            }
        }
    };

    Input.TextInputListener portInputListener = new Input.TextInputListener() {
        @Override
        public void input(String text) {
            Constants.CLIENT_PORT = Integer.parseInt(text);
            port = Integer.parseInt( text );
            setPortOk(true);
            synchronized ( ConnectionSettings.this ) {
                ConnectionSettings.this.notify();
            }
        }

        @Override
        public void canceled() {
            setPortOk(true);
            synchronized ( ConnectionSettings.this ) {
                ConnectionSettings.this.notify();
            }
        }
    };

    /**
     * constructor
     */
    public ConnectionSettings() {
        // set ip and port
        Gdx.input.getTextInput(ipInputListener, "Server IP", "localhost", "ip");
        Gdx.input.getTextInput(portInputListener, "Port", "8000", "port");
    }

    public synchronized boolean isIpOk() {
        return ipOk;
    }

    public synchronized void setIpOk(boolean ipOk) {
        this.ipOk = ipOk;
    }

    public synchronized boolean isPortOk() {
        return portOk;
    }

    public synchronized void setPortOk(boolean portOk) {
        this.portOk = portOk;
    }

    public String getIP() {

        Gdx.input.getTextInput(ipInputListener, "Server IP", "localhost", "ip");

        return IP;
    }

    public int getPort() {
        Gdx.input.getTextInput(portInputListener, "Port", "8000", "port");
        return port;
    }
}
