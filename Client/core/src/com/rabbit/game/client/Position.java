package com.rabbit.game.client;

public class Position {
	private float x, y;
	private int player_id;
	
	public Position(int player_id, float x, float y) {
		this.player_id = player_id;
		this.x = x;
		this.y = y;
	}


	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public int getPlayer_id() {
		return player_id;
	}
}
