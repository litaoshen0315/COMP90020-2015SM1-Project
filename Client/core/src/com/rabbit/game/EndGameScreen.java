package com.rabbit.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by litaoshen on 24/05/2015.
 */
public class EndGameScreen implements Screen {

    boolean win;
    Stage stage;
    SpriteBatch batch;
    BitmapFont font;
    RabbitHuntingGame game;

    public EndGameScreen(RabbitHuntingGame game, boolean win) {

        this.win = win;
        this.game = game;


    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        stage = new Stage();

    }

    @Override
    public void render(float v) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        font.setScale(2.0f);
        font.draw(batch, win ? "You win !!" : "You lose !!",
                 Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        stage.dispose();
        batch.dispose();
        font.dispose();

    }
}
