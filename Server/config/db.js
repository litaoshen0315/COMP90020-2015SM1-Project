// config/db.js
// database initialization

var path = require('path');
var async = require('async');
var log = require(path.join(__dirname, '..', 'utils', 'log.js'));

function delete_design(db, table_name, done) {
  'use strict';
  var table = db.use(table_name);

  table.get("_all_docs", {
    "startkey": "_design/",
    "endkey": "_design0",
    "include_docs": false
  }, function(error, body) {
    if (!error) {
      var design_docs = [];
      for (var i = 0; i < body.rows.length; i++) {
        design_docs.push({
          "_deleted": true,
          "_rev": body.rows[i].value.rev,
          "_id": body.rows[i].id
        });
      }

      table.bulk({
        "docs": design_docs
      }, function(error, body) {
        return done(error, body);
      });
    } else {
      return done(null);
    }
  });
}

module.exports = function(config, db, done) {
  var logger = log(config);

  // Create tables if not exists
  db.db.create(config.db.table.user);

  //delete_design(db, config.db.table.user, function done(error, body) {
  //callback(null, body);
  //});

  async.series([
    // Wipe design documents
    function(callback) {
      delete_design(db, config.db.table.user, function done(error, body) {
        callback(null, body);
      })
    }
  ], function(err, results) {
    done(err);
  });
}
