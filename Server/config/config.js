// config.js
// Exportable JSON config object

var seed_data = "true";

module.exports = {

  development: {
    name: 'Dev. REST Server',
    server: {
      host: 'localhost',
      port: 3000
    },
    db: {
      host: 'localhost',
      port: 5984,
      table: {
        user: 'user'
      }
    },
    log: true,
    seed: JSON.parse(seed_data),
  },

  test: {
    name: 'Test. REST Server',
    server: {
      host: 'localhost',
      port: 3000
    },
    db: {
      host: 'localhost',
      port: 5984,
      table: {
        user: 'user'
      }
    },
    log: true,
    seed: JSON.parse(seed_data),
  },

  production: {
    name: 'Prod. REST Server',
    server: {
      host: 'localhost',
      port: 3000
    },
    db: {
      host: 'localhost',
      port: 5984,
      table: {
        user: 'user'
      }
    },
    log: true,
    seed: JSON.parse(seed_data),
  }
}
