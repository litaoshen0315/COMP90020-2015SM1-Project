// config/seed.js
// Seeds database 

var async = require('async');
var path = require('path');
var log = require(path.join(__dirname, '..', 'utils', 'log.js'));

module.exports = function(config, db, done) {

  // Set up demo domain
  var demo_server = "http://127.0.0.1:8001";
  var domain = demo_server.substr(7).split(':')[0];

  var logger = log(config);

  if (!config.seed) {
    logger("Not Seeding Database");
    // Don't seed
    return done(null);
  }

  logger("[Seed] Database Seeding Started");

  var user = db.use(config.db.table.user);

  async.series([
    function(callback) {
      logger("[Seed] Destroying Old User Table");
      db.db.destroy(config.db.table.user, function(err) {
        callback(err, "Old Table Destruction");
      });
    },

    function(callback) {
      logger("[Seed] Creating User Table...");
      db.db.create(config.db.table.user, function(err) {
        callback(err, "User Table Creation");
      });
    },

    function(callback) {
      logger("[Seed] Reseeding User Table");
      var timestamp = new Date(Date.parse("2016-04-05T01:30:20.0002")).getTime();
      var users = [{
        "username": "admin",
        "email": "admin@rabbit.com",
        "password": "password",
        "score": 4
      }, {
        "username": "lance",
        "email": "lance@rabbit.com",
        "password": "password",
        "score": 5
      }];

      user.bulk({
        docs: users
      }, [], function(err, body) {
        if (err) {
          return callback(err);
        } else {
          callback(null, "Update Data");
        }
      });
    }
  ], function(err, results) {

    logger("[Seed] results: " + (err ? err : results) );
    logger("[Seed] Database Seeding Completed!");

    return done(err);

  });
}
