// app.js
// Initialise the routes and the entire application.

var http = require('http');
var path = require('path');

var async = require('async');
var express = require('express');
var bodyParser = require('body-parser');
var io = require('socket.io');
var redis = require('redis');
var nano = require('nano');

var log = require(path.join(__dirname, 'utils', 'log.js'));

module.exports = function(config, appdone) {

  'use strict';
  var logger = log(config);

  // Set up CouchDB (via Nano)
  var db_url = "http://" + config.db.host + ":" + config.db.port.toString();
  logger("Connecting to Database @" + db_url + "...");
  var db = nano(db_url);

  // Database (Intializationg --> Seeding --> Models/Views)
  async.series([
      function(callback) {
        // Initialize database
        logger('[Database] Database Initialization Started');
        require(path.join(__dirname, 'config', 'db.js'))(config, db,
          function(error) {
            if (error) {
              logger(error);
            } else {
              logger("[Database] Database Initialization Completed!");
            }
            callback(error, "Database Initialization");
          });
      },
      function(callback) {
        // Seeding
        logger('[Database] Seeding...');
        require(path.join(__dirname, 'config', 'seed.js'))(config, db,
          function(error) {
            if (error) {
              logger(error);
            } else {}
            callback(error, "Seeding");
          });
      },
      function(callback) {
        // Write CouchDB views to DB
        logger('[Database] Inserting CouchDB Views');
        require(path.join(__dirname, 'app', 'couchViews', 'couchViews.js'))
          (config, db,
            function(error) {
              if (error) {
                logger(error);
              } else {}
              callback(error, "Views Insertion");
            }
          );
      }
    ],
    function(err, results) {

      logger('[Database] results: ' + (err? err : results) );
      logger('[Database] Setup Complete!');


      // Set up ExpressJS App
      var app = express();

      // Body Parsing / CORS
      app.use(bodyParser.json({
        limit: '5mb'
      }));
      app.use(bodyParser.urlencoded({
        extended: true
      }));
      app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Authorization, Accept"
        );
        res.header('Access-Control-Allow-Methods',
          'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        next();
      });

      // 
      var server = http.createServer(app);
      var socketIo = io(server);
      var redisClient = redis.createClient();

      // Redis client connection
      redisClient.on('connect', function() {
        console.log('Redis connected');

        // TODO: Set up routes
        require(path.join(__dirname, 'routes', 'index.js'))(config, app,
          socketIo, redisClient);

        // Export App
        return appdone({
          start: function() {
            server.listen(config.server.port, function() {
              var port = server.address().port;
              logger("[App] Server Running @ localhost:" +
                port.toString());
            });
          },

          // Close database and server
          close: function() {
            var port = server.address().port;
            logger("[App] Server Shutting Down..." + port.toString());
            server.close();
          }
        });
      });

    });
}
