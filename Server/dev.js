// dev.js
// Entry point for development
// Initialize app and start up the server

var path = require('path');

// Initialize config
var config = require(path.join(__dirname, 'config', 'config.js'));

// Initialize App
require(path.join(__dirname, 'app.js'))(config.development, function(app){
  // Start
  app.start();
})
