// routes/index.js
// Contains root, other misc.

var path = require('path');
var async = require('async');
var log = require(path.join(__dirname, '..', 'utils', 'log.js'));

module.exports = function (config, app, io, client) {

  var logger = log(config);

  // Root
  app.get('/', function (req, res) {
    logger("GET /");

    res.send('Rabbit Hunting Game Server API');
  });

  // Socket.io connection
  io.on('connection', function(socket) {
    console.log('a user connected');
  });
}
