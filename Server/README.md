# Rabbit-Hunting-Game-Server #

This is a part of student project of COMP90020 Distributed Algorithms in the University of Melbourne at 2015 Semester 1

The centralised server for the Rabbit-Hunting-Game. Using the basic central server algorithm to handle the mutual exclustion problem.

## Run Locally ##

`npm start`

## Authors ##

* Xun Guo

* Litao Shen
