var path = require('path');
var log = require(path.join(__dirname, '..', '..', 'utils', 'log.js'));

var user_view = require(path.join(__dirname, 'user-view.js'));

module.exports = function(config, db, done) {
  
  var logger = log(config);

  (function(callback){

      logger("[Database] Inserting 'user' table views");
      user_view(config, db, function(error){
        return callback(error);
      });

  }(function(err){
    logger('[CouchViews] ' + (err? err : '') );
  }));

  
  return done(null);
}
