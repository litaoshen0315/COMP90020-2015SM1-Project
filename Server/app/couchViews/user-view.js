// app/models/user-view.js
// CouchDB views for users table

var path = require('path');
var async = require('async');
var log = require(path.join(__dirname, '..', '..', 'utils', 'log.js'));

module.exports = function(config, db, done) {

  logger = log(config);

  var user = db.use(config.db.table.user);

  // Get user by email and password
  user.insert({
      "views": {
        "by_email_pwd": {
          "map": function(doc) {
            emit([doc.email, doc.password], doc);
          }
        },
        "by_username_or_email": {
          "map": function(doc) {
            emit(doc.username, doc);
            emit(doc.email, doc);
          }
        }

      }
    }, '_design/auth', function(err, results) {
      if (err) {
        if (err.error != 'conflict') {
          console.log(err);
          logger('[User-View] ' + err);
        }
      } else {
        return done(null);
      }
    });
}
