package centralserver;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Protocol {
	
	@SuppressWarnings("unchecked")
	public static String posConnected(int player_count, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Connected");
		jObj.put("player_count", player_count);
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String sendPositionAndTargets( int player_id, SystemStatus systemStatus ) {

		boolean[] target_status = systemStatus.getTargetStatus();
		int num_targets = systemStatus.num_targets;
		ArrayList<Player> players = systemStatus.players;

		JSONObject jObj = new JSONObject();
		jObj.put("type", "Position");

		jObj.put("player", player_id);

		for (Player p: players) {
			jObj.put("x_pos" + p.id, p.getX_pos());
			jObj.put("y_pos" + p.id, p.getY_pos());
		}

		jObj.put( "num_targets", num_targets );
//		jObj.put("type", "TargetStatus");
		for (int i = 0; i < num_targets; i++) {
			jObj.put("target_status" + i, target_status[i]);
		}


		//System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String posDisconnect(int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Disconnect");
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String acceptRequest(int bridge_id, int player_id) {

		JSONObject jObj = new JSONObject();
		jObj.put("type", "Accept");
		jObj.put("bridge", bridge_id);
		jObj.put("player", player_id);

		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String waitingRequest(int bridge_id, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Waiting");
		jObj.put("bridge", bridge_id);
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String discardReceived(int bridge_id, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Discarded");
		jObj.put("bridge", bridge_id);
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String releaseReceived(int bridge_id, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Released");
		jObj.put("bridge", bridge_id);
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String acquiredTarget(boolean acquired, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Target");
		if (acquired) {
			jObj.put("target", "True");
		} else {
			jObj.put("target", "False");
		}
		jObj.put("player", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String returnTargetStatus(boolean[] target_status, int num_targets, int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "TargetStatus");
		for (int i = 0; i < num_targets; i++) {
			jObj.put("target_status" + i, target_status[i]);
		}
		jObj.put("player", player_id);
		
		// System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String returnUserId(int player_id) {
		
		JSONObject jObj = new JSONObject();
		jObj.put("type", "Joined");
		jObj.put("player_id", player_id);
		
		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String startGame(int player_count, int player_id) {

		JSONObject jObj = new JSONObject();
		jObj.put("type", "Start");
		jObj.put("player_count", player_count);
		jObj.put("player", player_id);
		jObj.put("init_x", Params.ENTRANCES[2 * (player_id - 1)]);
		jObj.put("init_y", Params.ENTRANCES[2 * (player_id - 1) + 1]);

		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String endGame(int winner_id, int player_id) {

		JSONObject jObj = new JSONObject();
		jObj.put("type", "End");
		jObj.put("winner", winner_id);
		jObj.put("player", player_id);

		System.out.println(jObj.toJSONString());
		return jObj.toJSONString();
	}

	public static JSONObject parse(String msg) {

		JSONParser parser = new JSONParser();
		JSONObject jObj = null;
		if (msg != null) {
			try {
				jObj = (JSONObject) parser.parse(msg);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return jObj;

	}

	public static String checkType(JSONObject jObj) {

		String type = "";

		if (jObj != null) {
			type = (String) jObj.get("type");
		}

		return type;
	}
}
