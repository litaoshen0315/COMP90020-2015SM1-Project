package centralserver;

public class Player {
	public int id;
	public boolean isConnected;
	
	private int score;
	private float x_pos;
	private float y_pos;
	private Bridge occupying;
	private Bridge waiting;
	
	public Player(int id) {
		this.id = id;
		this.isConnected = true;
		
		this.score = 0;
		this.setX_pos(Params.ENTRANCES[2 * (id - 1)]);
		this.setY_pos(Params.ENTRANCES[2 * (id - 1) + 1]);
		
		this.setOccupying(null);
		this.setWaiting(null);
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getScore() {
		return this.score;
	}

	public float getX_pos() {
		return x_pos;
	}

	public void setX_pos(float x_pos) {
		this.x_pos = x_pos;
	}

	public float getY_pos() {
		return y_pos;
	}

	public void setY_pos(float y_pos) {
		this.y_pos = y_pos;
	}

	public Bridge getOccupying() {
		return occupying;
	}

	public void setOccupying(Bridge occupying) {
		this.occupying = occupying;
	}

	public Bridge getWaiting() {
		return waiting;
	}

	public void setWaiting(Bridge waiting) {
		this.waiting = waiting;
	}
	
}
