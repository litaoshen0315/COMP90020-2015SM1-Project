package centralserver;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class PosConnection extends Thread {
	SystemStatus system_status;
	Socket socket;
	BufferedReader in;
	PrintWriter out;
	
	Player player;
	int player_id;
	int x_pos;
	int y_pos;
	
	public PosConnection(SystemStatus system_status, Socket socket, Player player) {
		this.system_status = system_status;
		this.socket = socket;
		this.player = player;
		
		try {
			this.socket.setSoTimeout(5000);
			this.in = new BufferedReader(new InputStreamReader(
					this.socket.getInputStream()));
			this.out = new PrintWriter(this.socket.getOutputStream(), true);
			
			out.println(Protocol.posConnected(this.system_status.getPlayerCount(), this.player.id));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public void run() {
		try {
			while (!system_status.getTimeout()) {
				if (this.isInterrupted() || socket == null
						|| socket.isClosed() || !this.player.isConnected) {
					break;
				}
				
				String playerPos = this.in.readLine();
				JSONObject jObj = Protocol.parse(playerPos);
				
				if (jObj == null) {
					throw new IOException();
				}

				//System.out.println(jObj.toJSONString());
				String header = Protocol.checkType(jObj);
				
				if (header.equals("Position")) {
					this.player.setX_pos(Float.parseFloat(jObj.get("x_pos").toString()));
					this.player.setY_pos(Float.parseFloat(jObj.get("y_pos").toString()));

					this.out.println( Protocol.sendPositionAndTargets(this.player.id, this.system_status) );
				} else if (header.equals("Leave")) {
					break;
				}
			}
		} catch (IOException e) {
			this.system_status.playerLost(this.player);
		} finally {		
			try {
				this.out.println(Protocol.posDisconnect(this.player.id));
				this.player.isConnected = false;
				this.socket.close();
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}
}
