package centralserver;

public class Target {
	public int id;
	public boolean acquired;
	public int belongs_to;
	
	public Target(int id) {
		this.id = id;
		this.acquired = false;
		this.belongs_to = 0;
	}
	
	public synchronized boolean acquire(int player) throws InterruptedException {
		if (this.belongs_to != 0) {
			return false;
		} else {
			this.acquired = true;
			this.belongs_to = player;
			notify();
			return true;
		}
	}
	
}
